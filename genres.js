const express = require('express');
const router = express.Router();

const genres = [
    { id: 1, name: "comedy" },
    { id: 2, name: "mystery" },
    { id: 3, name: "drama" },
];

function validateGenre(genre){
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(genre, schema);
};

// Get all genres
router.get('/', (req, res) => {
    res.send(genres);
});

// Get one genre
router.get('/:id', (req, res) => {
    // Lookup the genre or 404
    const genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre) return res.status(404).send('the genre with the given ID not found.');
    // If exist return
    res.send(genre);
});

// Create a Genre
router.post('/', (req, res) => {
    const { error } = validateGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    const genre = {
        id: genres.length + 1,
        name: req.body.name
    };
    genres.push(genre);
    res.send(genre);
});

// Modify a Genre
router.put('/:id', (req, res) => {
    // Lookup the genre or 404
    const genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre) return res.status(404).send('the genre with the given ID not found.');

    // Validate the request or 400
    const { error } = validateGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // update and return
    genre.name = req.body.name;
    res.send(genre);
});

// Delete a Genre
router.delete('/:id', (req, res) => {
    // Lookup the genre or 404
    const genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre) return res.status(404).send('the genre with the given ID not found.');

    // Delete and return
    const index = genres.indexOf(genre);
    genres.splice(index, 1);
    res.send(genre);
});

module.exports = router;
