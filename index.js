const express = require('express');
const Joi = require('joi');
const debug = require('debug')('app:startup');
const config = require('config');
const log = require('./logger');
const helmet = require("helmet");
const morgan = require('morgan');

const genres = require('./genres');
const home = require('./home');

// Express Initialization
const app = express();

// Middlewares
app.use(express.json());
app.use(helmet());
app.use(morgan('tiny'));
app.use(log);
app.use(express.static('./public'));

// Routes
app.use('/', home);
app.use('/api/genres', genres);

// Settings for PUG view engine
app.set('view engine', 'pug');
app.set('views', './views') // Default

// Working with NODE_ENV and app environment
console.log(`Node Env: ${process.env.NODE_ENV}`);
console.log(`App Env: ${app.get('env')}`);

// Env from config module
console.log(`App Name: ${config.get('name')}`);
console.log(`Mail Server: ${config.get('mail.host')}`);
console.log(`DB Host: ${config.get('db.host')}`);
// DB Password using config - custom environment variables 
if (app.get('env') === "development") console.log(`DB Password: ${config.get('db.dev_db_password')}`);
if (app.get('env') === "production") console.log(`DB Password: ${config.get('db.prod_db_password')}`);

// Debug Module
debug('Application is starting..');

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on port ${port}`));