const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    // res.send('<center> Hello Express </center>');
    res.render('index', {"title": "Express App", "message": "Hello Express"});
});

module.exports = router;